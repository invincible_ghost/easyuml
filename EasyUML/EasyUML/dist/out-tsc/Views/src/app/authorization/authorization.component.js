import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var AuthorizationComponent = /** @class */ (function () {
    function AuthorizationComponent() {
        this.display = true;
        this.display1 = false;
    }
    AuthorizationComponent.prototype.ngOnInit = function () {
    };
    AuthorizationComponent.prototype.showDialog = function () {
        this.display = true;
    };
    AuthorizationComponent.prototype.ShowSignUp = function () {
        this.display1 = true;
        this.display = false;
    };
    AuthorizationComponent = tslib_1.__decorate([
        Component({
            selector: 'app-authorization',
            templateUrl: './authorization.component.html',
            styleUrls: ['./authorization.component.sass']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], AuthorizationComponent);
    return AuthorizationComponent;
}());
export { AuthorizationComponent };
//# sourceMappingURL=authorization.component.js.map